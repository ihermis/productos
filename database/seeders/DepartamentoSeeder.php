<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DepartamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('departamentos')->insert([
            ['nombre' => 'Lavandería'],
            ['nombre' => 'Cocina'],
            ['nombre' => 'Almacen']
        ]);
    }
}
