<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MensajeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = "INSERT IGNORE INTO `mensajes` (`id`, `nombre`, `telefono`, `asunto`, `mail`, `mensaje`, `departamento_id`, `created_at`, `updated_at`) VALUES
        (1, 'Hermis', '112233', 'Prueba 1', 'hh@gg.com', 'prueba 1', 1, '2021-09-24 21:25:09', '2021-09-24 21:25:09'),
        (2, 'Hermis 2', '112222', 'Prueba 2', 'hh@gg.com', 'prueba 2', 1, '2021-09-24 21:32:52', '2021-09-24 21:32:52'),
        (3, 'Hemris 3', '11333', 'Prueba 3', 'hh@gg.com', 'prueba 3', 1, '2021-09-24 21:33:06', '2021-09-24 21:33:06'),
        (4, 'Javier', '23242', 'PRueba 4', 'hh@gg.com', 'PRueba 4', 3, '2021-09-24 21:41:50', '2021-09-24 21:41:50');";

        \DB::insert($sql);
    }
}
