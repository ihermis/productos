<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert([
            ['id' => 1 ,
            'name' => 'Administrador',
            'email' => 'ht@gg.com',
            'password' => bcrypt('123123'),
            'rol_id' => 1,
            ]
        ]);
    }
}
