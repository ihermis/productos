<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Departamento;
class DepartamentoController extends Controller
{
    //
    public function index()
    {
        $departamentos = Departamento::with('mensajes')->get();

        return view('departamento.listado')->with('departamentos' , $departamentos);
    }

    public function delete($id)
    {
        $departamento = Departamento::find($id);
        $departamento->delete();

        return redirect('/lista-departamentos');
    }
}
