<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mensaje;
use App\Models\Departamento;

class MensajeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    
    }
    public function index()
    {           
        //
    }

    public function crearRegistro()
    {
        $departamentos = Departamento::all();
        return view('mensaje.form')->with('departamentos' , $departamentos);
    }
    
    public function editarRegistro($id)
    {
    
        $mensaje = Mensaje::find($id);
        $departamentos = Departamento::all();
        return view('mensaje.form-editar')->with(['mensaje' => $mensaje , 'departamentos' => $departamentos]);
    }

    public function save(Request $request)
    {
        $mensaje =  new Mensaje();
        $mensaje->nombre = $request->nombre;
        $mensaje->asunto = $request->asunto;
        $mensaje->telefono = $request->telefono;
        $mensaje->mail = $request->mail;
        $mensaje->mensaje = $request->mensaje;
        $mensaje->departamento_id = $request->departamento;
        $mensaje->save();
        return redirect('/');

    }

    public function update(Request $request)
    {
        $mensaje = Mensaje::find($request->id);
        $mensaje->nombre = $request->nombre;
        $mensaje->asunto = $request->asunto;
        $mensaje->telefono = $request->telefono;
        $mensaje->mail = $request->mail;
        $mensaje->mensaje = $request->mensaje;
        $mensaje->departamento_id = $request->departamento;
        $mensaje->save();
        return redirect('/');

    }

    public function delete($id)
    {
        $mensaje = Mensaje::find($id);
        $mensaje->delete();
        return redirect('/');

    }

    public function listaEnviados()
    {
        $mensaje = Mensaje::with('departamento')->get();
    
        return view('mensaje.listado')->with('mensajes' , $mensaje);
    }
}
