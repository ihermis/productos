<?php

namespace App\Http\Controllers;

use App\Models\Producto;
use Illuminate\Http\Request;
use App\Models\Departamento;
use Illuminate\Support\Facades\File;
use App\Events\NuevoProducto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    
    }

    public function index()
    {
        $productos = Producto::with('departamento')->get();
        return view('producto.index')->with('productos' , $productos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departamentos = Departamento::all();
        return view('producto.create')->with('departamentos' , $departamentos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $producto = new Producto();
        $producto->nombre = $request->input('nombre');
        $producto->codigo = $request->codigo;
        $producto->precio = $request->get('precio');
        $producto->departamento_id = $request->get('departamento');
        $producto->user_id = auth()->user()->id;        
        

        if($producto->save()){
            
            if($request->hasFile('imagen'))
            {
                $imagenes = $request->file('imagen');

                foreach ($imagenes as $imagen) {
                    $path = 'images/productos/' . $producto->id . '/';
                    $name = time() . '-' . $imagen->getClientOriginalName();

                    $imagen->move($path , $name);

                    $images[] = $name;
                }                

                $producto->imagen = implode('|' , $images);

                $producto->save();
            }
        }
        // event(new NuevoProducto($producto));
        // die;
        return redirect('productos');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(Producto $producto)
    {
        return view('producto.show')->with('producto' , $producto);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit(Producto $producto)
    {
        $departamentos = Departamento::all();
        return view('producto.edit')->with(['producto' => $producto , 'departamentos' => $departamentos]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $producto)
    {

        $producto->nombre = $request->input('nombre');
        $producto->codigo = $request->codigo;
        $producto->precio = $request->get('precio');
        $producto->departamento_id = $request->get('departamento');

        $producto->save();

        return redirect('productos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {
        
        $path = public_path('images/productos/' . $producto->id);
        if($producto->delete()){
            File::deleteDirectory($path);
        }

        return redirect('productos');
    }
}
