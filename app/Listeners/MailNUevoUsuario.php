<?php

namespace App\Listeners;

use App\Events\NuevoUsuario;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class MailNUevoUsuario
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NuevoUsuario  $event
     * @return void
     */
    public function handle(NuevoUsuario $event)
    {
        //
    }
}
