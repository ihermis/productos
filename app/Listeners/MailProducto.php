<?php

namespace App\Listeners;

use App\Events\NuevoProducto;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Mail\MailNuevoProducto;

class MailProducto
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NuevoProducto  $event
     * @return void
     */
    public function handle(NuevoProducto $event)
    {
        new MailNuevoProducto($event->producto);
    }
}
