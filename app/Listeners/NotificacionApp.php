<?php

namespace App\Listeners;

use App\Events\NuevoProducto;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotificacionApp
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NuevoProducto  $event
     * @return void
     */
    public function handle(NuevoProducto $event)
    {
        print_r('Nuevo producto, precio:: ' . $event->producto->precio);
    }
}
