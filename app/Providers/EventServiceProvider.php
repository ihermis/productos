<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

use App\Events\NuevoUsuario;
use App\Events\NuevoProducto;
use App\Listeners\MailNUevoUsuario;
use App\Listeners\NotificacionApp;
use App\Listeners\MailProducto;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        NuevoUsuario::class => [
            MailNUevoUsuario::class,
            NotificacionApp::class
        ],
        NuevoProducto::class => [
            MailProducto::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
