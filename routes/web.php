<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MensajeController;
use App\Http\Controllers\DepartamentoController;
use App\Http\Controllers\ProductoController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/enviar-mensaje' , [MensajeController::class , 'save']);
//Route::get('/' , [MensajeController::class , 'listaEnviados']);
Route::get('/crear-registro' , [MensajeController::class , 'crearRegistro']);
Route::get('/editar-registro/{id}' , [MensajeController::class , 'editarRegistro']);
Route::put('/editar-mensaje/{id}' , [MensajeController::class , 'update']);
Route::delete('/eliminar-registro/{id}' , [MensajeController::class , 'delete']);

Route::get('/lista-departamentos' , [DepartamentoController::class , 'index'])->name('depa');
Route::delete('/eliminar-depa/{id}' , [DepartamentoController::class , 'delete']);
Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

#Productos
Route::resource('productos' , ProductoController::class);