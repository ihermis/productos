<nav class="navbar navbar-expand-lg navbar-light bg-light">
<div class="container-fluid">
        <a class="navbar-brand" href="#">Test</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav">
        {{-- <li class="nav-item">
        <a class="nav-link active" aria-current="page" href="/crear-registro">Formulario</a>
        </li> --}}
        <li class="nav-item">
        <a class="nav-link" href="/">Productos</a>
        </li>
        <li class="nav-item">
        <a class="nav-link" href="{{ route('depa')}} ">Departamentos</a>
        </li>

        
        </ul>
        </div>

        <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @endif

                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else
                    <li class="nav-item dropdown">
                
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Salir') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
              
                    </li>
                @endguest
            </ul>
</div>
</nav>