@extends('layouts.app')
@section('content')
<div class="container">
    <form action="/productos/{{ $producto->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="nombre" class="form-label">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="{{ $producto->nombre }}">
        </div>
        <div class="mb-3">
            <label for="codigo" class="form-label">Código</label>
            <input type="text" class="form-control" id="codigo" name="codigo" placeholder="" value="{{ $producto->codigo }}">
        </div>
        <div class="mb-3">
            <label for="precio" class="form-label">Precio</label>
            <input type="number" class="form-control" id="precio" name="precio" placeholder="" value="{{ $producto->precio }}">
        </div>
       
        <div class="mb-3">
            <label for="departamento" class="form-label">Departamento</label>
            <select class="form-control" id="departamento" name="departamento">
                @foreach ($departamentos as $depa)
                    @if ($depa->id == $producto->departamento->id)
                        <option value="{{ $depa->id }}" selected>{{ $depa->nombre }}</option>
                    @else
                        <option value="{{ $depa->id }}">{{ $depa->nombre }}</option>
                    @endif                        
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
    </form>
</div>
@endsection