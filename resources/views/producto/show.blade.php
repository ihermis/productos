@extends('layouts.app')
@section('content')
<div class="container">
    <form action="/" method="">
        @csrf
        <div class="mb-3">
            <label for="nombre" class="form-label">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="{{ $producto->nombre }}" disabled>
        </div>
        <div class="mb-3">
            <label for="codigo" class="form-label">Código</label>
            <input type="text" class="form-control" id="codigo" name="codigo" placeholder="" value="{{ $producto->codigo }}" disabled>
        </div>
        <div class="mb-3">
            <label for="precio" class="form-label">Precio</label>
            <input type="number" class="form-control" id="precio" name="precio" placeholder="" value="{{ $producto->precio }}" disabled>
        </div>
       
        <div class="mb-3">
            <label for="departamento" class="form-label">Departamento</label>
            
            @if ($producto->departamento)
                <strong>{{ $producto->departamento->nombre }}</strong>
            @else
                <span>Sin Departamento</span>
            @endif
            
        </div>
    </form>
</div>
@endsection