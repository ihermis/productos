@extends('layouts.app')
@section('content')
    <div class="container">
        <h2>Productos</h2>
        <br>
        <a href="/productos/create" class="btn btn-primary">Agregar Producto</a>
        <table class="table">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Código</th>
                <th scope="col">Precio</th>
                <th scope="col">Departamento</th>
                <th scope="col">Imagen</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
                @foreach ($productos as $producto)
                    <tr>
                        <th scope="row">{{$producto->id}}</th>
                        <td>{{$producto->nombre}}</td>
                        <td>{{$producto->codigo}}</td>
                        <td>{{$producto->precio}}</td>
                        <td><img src="{{ asset('images/productos/'.$producto->id.'/'.$producto->imagen)}}" alt="{{$producto->nombre}}" style="width: 80px; height:auto;"></td>
                        <td>
                          @if ($producto->departamento)
                            {{$producto->departamento->nombre}}
                          @else
                              Sin departamento
                          @endif
                          
                        </td>
                        <th>
                          <form action="/productos/{{$producto->id}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="/productos/{{$producto->id}}" class="btn btn-primary">Ver</a>
                            <a href="/productos/{{$producto->id}}/edit" class="btn btn-warning">Editar</a>
                            <button type="submit" class="btn btn-danger">Eliminar</button>
                          </form>
                        </th>
                  </tr>
                @endforeach
             
            </tbody>
          </table>
          <example-component></example-component>
    </div>
@endsection