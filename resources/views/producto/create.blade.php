@extends('layouts.app')
@section('content')
<div class="container">
    <form action="/productos" method="POST" enctype="multipart/form-data">
        @csrf
        {{-- <input type="hidden" value="{{ Auth::user()->id }}"> --}}
        <div class="mb-3">
            <label for="nombre" class="form-label">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="">
        </div>
        <div class="mb-3">
            <label for="codigo" class="form-label">Código</label>
            <input type="text" class="form-control" id="codigo" name="codigo" placeholder="" value="">
        </div>
        <div class="mb-3">
            <label for="precio" class="form-label">Precio</label>
            <input type="number" class="form-control" id="precio" name="precio" placeholder="" value="">
        </div>
        <div class="mb-3">
            <label for="imagen" class="form-label">Imagen</label>
            <input type="file" class="form-control" id="imagen" name="imagen[]" placeholder="" value="">
        </div>

       
        <div class="mb-3">
            <label for="departamento" class="form-label">Departamento</label>
            <select class="form-control" id="departamento" name="departamento">
                @foreach ($departamentos as $depa)
                    <option value="{{ $depa->id }}">{{ $depa->nombre }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
    </form>
</div>
@endsection