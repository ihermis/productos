@include('layout.header')
@include('layout.nav')

<div class="container">
  <br>
    <a href="/crear-registro" class="btn btn-primary">Crear</a>
    <br>
    <table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nombre</th>
            <th scope="col">Teléfono</th>
            <th scope="col">Asunto</th>
            <th scope="col">Mail</th>
            <th scope="col">Departamento</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
            @foreach ($mensajes as $mensaje)
                <tr>
                    <th scope="row">{{$mensaje->id}}</th>
                    <td>{{$mensaje->nombre}}</td>
                    <td>{{$mensaje->telefono}}</td>
                    <td>{{$mensaje->asunto}}</td>
                    <td>{{$mensaje->mail}}</td>
                    <td>
                      @if ($mensaje->departamento)
                        {{$mensaje->departamento->nombre}}
                      @else
                          Sin departamento
                      @endif
                      
                    </td>
                    <th>
                      <form action="/eliminar-registro/{{$mensaje->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <a href="/editar-registro/{{$mensaje->id}}" class="btn btn-warning">Editar</a>
                        <button type="submit" class="btn btn-danger">Eliminar</button>
                      </form>
                    </th>
              </tr>
            @endforeach
         
        </tbody>
      </table>
</div>
@include('layout.footer')