@include('layout.header')
@include('layout.nav')
<div class="container">
    <form action="/editar-mensaje/{{$mensaje->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="nombre" class="form-label">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="" value="{{$mensaje->nombre}}">
        </div>
        <div class="mb-3">
            <label for="telefono" class="form-label">Teléfono</label>
            <input type="text" class="form-control" id="telefono" name="telefono" placeholder="" value="{{$mensaje->telefono}}">
        </div>
        <div class="mb-3">
            <label for="asunto" class="form-label">Asunto</label>
            <input type="text" class="form-control" id="asunto" name="asunto" placeholder="" value="{{$mensaje->asunto}}">
        </div>
        <div class="mb-3">
            <label for="mail" class="form-label">Correo de mensaje</label>
            <input type="mail" class="form-control" id="mail" name="mail" placeholder="" value="{{$mensaje->mail}}">
        </div>
        <div class="mb-3">
            <label for="mensaje" class="form-label">Mensaje</label>
            <textarea class="form-control" id="mensaje" name="mensaje" erows="3">{{$mensaje->mensaje}}</textarea>
        </div>
        <div class="mb-3">
            <label for="departamento" class="form-label">Departamento</label>
            <select class="form-control" id="departamento" name="departamento">
                @foreach ($departamentos as $depa)
                    <option value="{{ $depa->id }}">{{ $depa->nombre }}</option>
                @endforeach
            </select>
        </div>
        <div class="mb-3">
            <button type="submit" class="btn btn-primary">Editar</button>
        </div>
    </form>
</div>
  
@include('layout.footer')