@extends('layouts.app')
@section('content')
<div class="container">
  <br>
    <a href="/crear-registro" class="btn btn-primary">Crear</a>
    <br>
    @foreach ($departamentos as $depa)  
    <br>      
    <h4>{{ $depa->nombre }} 
    <br>
    <form action="/eliminar-depa/{{$depa->id}}" method="POST">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">Eliminar</button>
      </form>
    </h4>
    <br>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Nombre</th>
                <th scope="col">Teléfono</th>
                <th scope="col">Asunto</th>
                <th scope="col">Mail</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($depa->mensajes as $mensaje)
                    <tr>
                        <th scope="row">{{$mensaje->id}}</th>
                        <td>{{$mensaje->nombre}}</td>
                        <td>{{$mensaje->telefono}}</td>
                        <td>{{$mensaje->asunto}}</td>
                        <td>{{$mensaje->mail}}</td>
                </tr>
                @endforeach
            
            </tbody>
        </table>
      @endforeach
</div>
@endsection